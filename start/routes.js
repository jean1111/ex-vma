'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route');

// Route.on('/').render('welcome')
Route.group(() => {
  Route.post('user/login', 'UserController.login');
  Route.resource('user', 'UserController')
    .middleware(new Map([
      [['index', 'show', 'store', 'update', 'destroy'], ['auth']],
    ]));
  Route.put('exalumnas/changevm/:id', 'ExAlumnasController.changeVM');
  Route.put('exalumnas/changesocia/:id', 'ExAlumnasController.changeSOCIA');
  Route.put('exalumnas/changevitalicia/:id', 'ExAlumnasController.changeVITALICIA');
  Route.resource('exalumnas', 'ExAlumnasController')
    .middleware(new Map([
      [['index', 'show', 'store', 'update', 'destroy'], ['auth']],
    ]));
  Route.resource('regiones', 'RegionController')
    .middleware(new Map([
      [['index', 'show'], ['auth']],
    ]));
}).prefix('api');


Route.any('*', ({ view }) => view.render('index'));
