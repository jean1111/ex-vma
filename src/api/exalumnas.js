import axios from 'axios';
import { apiUrl } from './';

export default {
  async fetch(params) {
    return axios.get(`${apiUrl}/exalumnas`, { params });
  },
  async create(params) {
    return axios.post(`${apiUrl}/exalumnas`, params);
  },
  async update(id, params) {
    return axios.put(`${apiUrl}/exalumnas/${id}`, params);
  },
  async delete(id) {
    return axios.delete(`${apiUrl}/exalumnas/${id}`);
  },
  async regionesall(params) {
    return axios.get(`${apiUrl}/regiones`, { params });
  },
  async changevm(id) {
    return axios.put(`${apiUrl}/exalumnas/changevm/${id}`);
  },
  async changesocia(id) {
    return axios.put(`${apiUrl}/exalumnas/changesocia/${id}`);
  },
  async changevitalicia(id) {
    return axios.put(`${apiUrl}/exalumnas/changevitalicia/${id}`);
  },
};
