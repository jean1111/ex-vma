import Index from '../pages/Index';
import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/home',
      name: 'home',
      component: Index,
      beforeEnter: requireAuth,
    },
  ],
}];
