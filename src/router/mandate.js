import Mandate from '../pages/mandate/Mandate';
import Layout from '../layouts/Layout';
// import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/mandate',
      name: 'mandate',
      component: Mandate,
      /* beforeEnter: requireAuth, */
    },
  ],
}];
