import ExStudents from '../pages/students/ExStudent';
import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/ex-students',
      name: 'ex-students',
      component: ExStudents,
      beforeEnter: requireAuth,
    },
  ],
}];
