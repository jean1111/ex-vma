import Router from 'vue-router';
import { getJsModules } from '../plugins/util';

const modules = getJsModules(require.context('.', false, /\.js$/), true);

const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: modules,
});

export default router;
