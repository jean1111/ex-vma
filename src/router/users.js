import Users from '../pages/users/Users';
import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/users',
      name: 'users',
      component: Users,
      beforeEnter: requireAuth,
    },
  ],
}];
