import Stock from '../pages/stock/Stock';
import Layout from '../layouts/Layout';
// import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/stock',
      name: 'stock',
      component: Stock,
      /* beforeEnter: requireAuth, */
    },
  ],
}];
