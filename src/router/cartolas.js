import Cartolas from '../pages/cartolas/Cartolas';
import Layout from '../layouts/Layout';
// import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/cartolas',
      name: 'cartolas',
      component: Cartolas,
      /* beforeEnter: requireAuth, */
    },
  ],
}];
