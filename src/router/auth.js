import LayoutEmpty from '../layouts/LayoutEmpty';
import Login from '../pages/auth/Login';

export default [{
  path: '/auth',
  component: LayoutEmpty,
  children: [
    {
      path: 'login',
      name: 'login',
      component: Login,
    },
  ],
}];
