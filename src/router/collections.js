import Collections from '../pages/collection/Collections';
import Layout from '../layouts/Layout';
// import { requireAuth } from '../plugins/util';

export default [{
  path: '/',
  component: Layout,
  children: [
    {
      path: '/collections',
      name: 'collections',
      component: Collections,
      /* beforeEnter: requireAuth, */
    },
  ],
}];
