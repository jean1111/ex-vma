export default class UserM {
  constructor(user) {
    if (user) {
      const {
        id = null,
        username = null,
        email = null,
      } = user;
      this.id = id;
      this.username = username;
      this.email = email;
    } else {
      this.id = null;
      this.username = null;
      this.email = null;
    }
  }
}
