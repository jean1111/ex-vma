export default class ExAlumna {
  constructor(exvma) {
    if (exvma) {
      const {
        id = null,
        rut = null,
        email = null,
        nombre = null,
        tlf_fijo = null,
        apellido_p = null,
        tlf_celular = null,
        apellido_m = null,
        profesion = null,
        pais = null,
        region = null,
        lider_genrn = null,
        direccion = null,
        generacion = null,
        comuna = null,
        w_m = null,
        socia = null,
        vitalicia = null,
        observaciones = null,
      } = exvma;
      this.id = id;
      this.rut = rut;
      this.email = email;
      this.nombre = nombre;
      this.tlf_fijo = tlf_fijo;
      this.apellido_p = apellido_p;
      this.tlf_celular = tlf_celular;
      this.apellido_m = apellido_m;
      this.profesion = profesion;
      this.pais = pais;
      this.region = region;
      this.lider_genrn = lider_genrn;
      this.direccion = direccion;
      this.generacion = generacion;
      this.comuna = comuna;
      this.w_m = w_m;
      this.socia = socia;
      this.vitalicia = vitalicia;
      this.observaciones = observaciones;
    } else {
      this.id = null;
      this.rut = null;
      this.email = null;
      this.nombre = null;
      this.tlf_fijo = null;
      this.apellido_p = null;
      this.tlf_celular = null;
      this.apellido_m = null;
      this.profesion = null;
      this.pais = null;
      this.region = null;
      this.lider_genrn = null;
      this.direccion = null;
      this.generacion = null;
      this.comuna = null;
      this.w_m = null;
      this.socia = null;
      this.vitalicia = null;
      this.observaciones = null;
    }
  }
}
