import moment from 'moment';
import auth from '../store/auth';

/**
 * Method to get js default modules
 * @param files
 * @param isArray
 * @returns {*}
 */
export function getJsModules(files, isArray = false) {
  const modules = isArray ? [] : {};
  files.keys().forEach((key) => {
    if (key === './index.js') return;
    if (isArray) files(key).default.map(r => modules.push(r));
    else modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default;
  });
  return modules;
}

export function getScore(value) {
  const val = (value / 1).toFixed(2).replace('.', ',');
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

/**
 * Method for protect routes
 * @param Component
 * @param rest
 * @returns {*}
 */

export function requireAuth(to, from, next) {
  if (!auth.getLocalUser()) {
    next({
      path: '/auth/login',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
}

export const rules = {
  empty: v => !!v || 'This field is required',
  number: v => /^\d+$/.test(v) || 'Only number',
  float: v => /^\d+(\.\d{1,4})?$/.test(v) || 'Only number',
  email: v => /\S+@\S+\.\S+/.test(v) || 'Email must be valid.',
  password: v => (v && v.length >= 8) || 'Password must include at least 8 characters',
  afterDate: (v, before) => !moment(before).subtract(0, 'days').isBefore(v) || 'Incorrect Date',
  afterTime: (v, before) => !moment(before, 'HH:mm').isBefore(moment(v, 'HH:mm')) || 'Incorrect Time',
  maxPercentage: v => v <= 100 || 'Incorrect porcentaje',
};

export function generateDealCode(id, charterer = '') {
  const last = (id === 0 || id === -Infinity ? 1 : id).toString();
  const temp = '000';
  return `${charterer && `${charterer.substring(0, 2).toUpperCase()}-`}${temp.substring(0, temp.length - last.toString().length) + last}`;
}

export function getPortById(ports, id) {
  if (ports) {
    const port = this.ports.filter((v) => {
      return v.value === id;
    });
    if (port.length > 0) return port[0].text;
  }
  return '';
}

export function getUserById(users, id) {
  if (users) {
    const user = this.users.filter((v) => {
      return v.value === id;
    });
    if (user.length > 0) return user[0].text;
  }
  return '';
}

export function Validatefiles(files) {
  if (files) {
    const FileSize = this.files[0].size / 1024 / 1024;
    if (FileSize > 2) {
      this.errorFile = 'File size should be less than 2MB';
    } else {
      this.errorFile = '';
    }
  }
}

export function thousandFormat(number) {
  if (number) {
    return parseFloat(number).toLocaleString('en');
  }
  return 0;
}

export function getScheduleDates(schedule) {
  const arr = [];
  if (schedule) {
    schedule.forEach((s) => {
      arr.push(new Date(`${s.etaDate_updated} ${s.etaTime_updated}`));
    });
  }
  return arr;
}

export function canContinue(t, loadDate, dischargeDate) {
  let flag_timer = false;
  const nw = new Date();
  if (t.before_after === 1 && t.event === 1) { //Before load
    if (nw.getTime() < loadDate.getTime()) flag_timer = true;
  } else if (t.before_after === 2 && t.event === 1) { //After load
    if (nw.getTime() >= loadDate.getTime()) flag_timer = true;
  } else if (t.before_after === 1 && t.event === 2) { //Before discharge
    if (nw.getTime() < dischargeDate.getTime()) flag_timer = true;
  } else if (t.before_after === 2 && t.event === 2) { //After discharge
    if (nw.getTime() >= dischargeDate.getTime()) flag_timer = true;
  }
  return flag_timer;
}

export function getFlagDate(t, loadDate, dischargeDate) {
  let flag_date = null;
  if (t.before_after === 1 && t.event === 1) { //Before load
    flag_date = loadDate.setHours(loadDate.getHours() - t.deadline);
  } else if (t.before_after === 2 && t.event === 1) { //After load
    flag_date = loadDate.setHours(loadDate.getHours() + t.deadline);
  } else if (t.before_after === 1 && t.event === 2) { //Before discharge
    flag_date = dischargeDate.setHours(dischargeDate.getHours() - t.deadline);
  } else if (t.before_after === 2 && t.event === 2) { //After discharge
    flag_date = dischargeDate.setHours(dischargeDate.getHours() + t.deadline);
  }
  flag_date = new Date(flag_date);
  return flag_date.getTime();
}
