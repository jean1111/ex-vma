'use strict'

const ExAlumna = use('App/Models/ExAlumna');
const moment = require('moment');

class ExAlumnasController {

  async index ({ request, response, view }) {
    const count = await ExAlumna.query().where('socia', 1).count();
    const socias = count[0]['count(*)'];
    const {
      page = 1,
      rowsPerPage = 10,
      sortBy = 'id',
      descending = 'desc',
      search = '',
    } = request.all();
      const exalumnas = await ExAlumna.query()
      /* .whereRaw('email LIKE ?', [`%${search}%`])
      .whereRaw('rut LIKE ?', [`%${search}%`]) */
      .whereRaw('nombre LIKE ?', [`%${search}%`])
      .orderBy(sortBy, descending)
      .paginate(page, rowsPerPage === '-1' ? 99999999999 : rowsPerPage);
      return { exalumnas: exalumnas, socias: socias };
  }

  async create ({ request, response, view }) {
  }

  async store ({ request, response }) {
    const exvma = request.only([
      'rut',
      'email',
      'nombre',
      'tlf_fijo',
      'apellido_p',
      'tlf_celular',
      'apellido_m',
      'profesion',
      'pais',
      'region',
      'lider_genrn',
      'direccion',
      'generacion',
      'comuna',
      'w_m',
      'socia',
      'vitalicia',
      'observaciones'
    ]);
    exvma.user_id = 1;
    exvma.w_m = exvma.w_m === 'true' ? 1 : 0;
    exvma.socia = exvma.socia === 'true' ? 1 : 0;
    exvma.vitalicia = exvma.vitalicia === 'true' ? 1 : 0;
    const result = await ExAlumna.create(exvma);
    return;
  }

  async show ({ params, request, response, view }) {
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response }) {
    const { action } = request.only(['action']);
    if (action === 'delete') {
      const exv = await ExAlumna.find(params.id);
      await exv.delete();
    } else {
      const now = moment().format('YYYY-MM-DD HH:mm');
      const exvma = request.all();
      if (exvma.created_at === 'null') {
        exvma.created_at = now;
      }
      if (exvma.updated_at === 'null') {
        exvma.updated_at = now;
      }
      const exv = await ExAlumna.find(exvma.id);
      exv.merge(exvma);
      await exv.save();
    }
    return;
  }

  async destroy ({ params, request, response }) {
    const exv = await ExAlumna.find(params.id);
    await exv.delete();
    return;
  }

  async changeVM ({ params, request, response, auth }) {
    const exv = await ExAlumna.find(params.id);
    if (exv.w_m === 0) {
      exv.w_m = 1;
    } else {
      exv.w_m = 0;
    }
    exv.save();
    return exv;
  }
  async changeSOCIA ({ params, request, response, auth }) {
    const exv = await ExAlumna.find(params.id);
    if (exv.socia === 0) {
      exv.socia = 1;
    } else {
      exv.socia = 0;
    }
    exv.save();
    return exv;
  }
  async changeVITALICIA ({ params, request, response, auth }) {
    const exv = await ExAlumna.find(params.id);
    if (exv.vitalicia === 0) {
      exv.vitalicia = 1;
    } else {
      exv.vitalicia = 0;
    }
    exv.save();
    return exv;
  }
}

module.exports = ExAlumnasController
