'use strict'
const User = use('App/Models/User');
/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   */
  async index({ request }) {
    /* const {
      page = 1,
      rowsPerPage = 10,
      sortBy = 'username',
      descending = 'desc',
      search = '',
    } = request.all();
    const users = await User.query()
      .whereRaw('email LIKE ?', [`%${search}%`])
      .orderBy(sortBy, descending)
      .paginate(page, rowsPerPage === '-1' ? 99999999999 : rowsPerPage);
    return users; */
    return await User.query().orderBy('id', 'desc').fetch();
  }

  /**
   * Render a form to be used for creating a new user.
   * GET users/create
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new user.
   * POST users
   */
  async store({ request, response }) {
    //obtiene los datos enviados
    const { email, password, username } = request.all();
    //Se guardan en la base de datos
    const user = await User.create({
      email,
      password,
      username
    });
    // response.send(user);
    return;
  }

  /**
   * Display a single user.
   * GET users/:id
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   */
  async update({ params, request, response }) {
    const { action } = request.only(['action']);
    if (action === 'delete') {
      const user = await User.find(params.id);
      await user.delete();
    } else {
      const users = request.only(['id', 'username', 'email']);
      const user = await User.find(users.id);
      user.merge(users);
      await user.save();
    }
    return;
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   */
  async destroy({ params, request, response }) {
    const user = await User.find(params.id);
    await user.delete();
    return;
  }

  async login({ request, auth }) {
    const { email, password } = request.all();
    const token = await auth
      .withRefreshToken()
      .attempt(email, password, true);

    return token;
  }
}
module.exports = UserController;
