'use strict'
const Region = use('App/Models/Region');
/**
 * Resourceful controller for interacting with regions
 */
class RegionController {
  /**
   * Show a list of all regions.
   * GET regions
   */
  async index ({ request, response, view }) {
    return await Region.query().orderBy('id', 'desc').fetch();
  }

  /**
   * Render a form to be used for creating a new region.
   * GET regions/create
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new region.
   * POST regions
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single region.
   * GET regions/:id
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing region.
   * GET regions/:id/edit
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update region details.
   * PUT or PATCH regions/:id
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a region with id.
   * DELETE regions/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = RegionController
