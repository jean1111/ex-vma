'use strict'

const Model = use('Model')

class ExAlumna extends Model {

  static get table() {
    return 'exalumnas';
  }

}

module.exports = ExAlumna
