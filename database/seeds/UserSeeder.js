'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const User = use('App/Models/User');
//const Factory = use('Factory');


class UserSeeder {
  async run() {
    await User.create({ username: 'admin', email: 'admin@admin.com', password: '123456789' });
  }
}

module.exports = UserSeeder;
