'use strict';

/*
|--------------------------------------------------------------------------
| RegionesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Region = use('App/Models/Region');

class RegionesSeeder {
 async run() {
   const regiones = [
     {
       name: 'Arica y Parinacota',
       ordinal: 'XV',
     },
     {
       name: 'Tarapacá',
       ordinal: 'I',
     },
     {
       name: 'Antofagasta',
       ordinal: 'II',
     },
     {
       name: 'Atacama',
       ordinal: 'III',
     },
     {
       name: 'Coquimbo',
       ordinal: 'IV',
     },
     {
       name: 'Valparaiso',
       ordinal: 'V',
     },
     {
       name: 'Metropolitana de Santiago',
       ordinal: 'RM',
     },
     {
       name: 'Libertador General Bernardo O\'Higgins',
       ordinal: 'VI',
     },
     {
       name: 'Maule',
       ordinal: 'VII',
     },
     {
       name: 'Biobío',
       ordinal: 'VIII',
     },
     {
       name: 'La Araucanía',
       ordinal: 'IX',
     },
     {
       name: 'Los Ríos',
       ordinal: 'XIV',
     },
     {
       name: 'Los Lagos',
       ordinal: 'X',
     },
     {
       name: 'Aisén del General Carlos Ibáñez del Campo',
       ordinal: 'XI',
     },
     {
       name: 'Magallanes y de la Antártica Chilena',
       ordinal: 'XII',
     },
   ];

   await Region.createMany(regiones);
 }
}

module.exports = RegionesSeeder;
