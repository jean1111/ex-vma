'use strict'

const Schema = use('Schema')

class ExalumnasSchema extends Schema {
  up () {
    this.create('exalumnas', (table) => {
      table.increments()
      table.string('rut', 100)
      table.string('email', 100)
      table.string('nombre', 100)
      table.string('tlf_fijo', 100)
      table.string('apellido_p', 100)
      table.string('tlf_celular', 100)
      table.string('apellido_m', 100)
      table.string('profesion', 100)
      table.string('pais', 100)
      table.string('region', 100)
      table.string('lider_genrn', 2).defaultTo('no')
      table.text('direccion')
      table.string('generacion', 100)
      table.string('comuna', 100)
      table.boolean('w_m').defaultTo(0)
      table.boolean('socia').defaultTo(0)
      table.boolean('vitalicia').defaultTo(0)
      table.text('observaciones')
      table.integer('user_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('exalumnas')
  }
}

module.exports = ExalumnasSchema
