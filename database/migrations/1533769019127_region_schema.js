'use strict'

const Schema = use('Schema')

class RegionSchema extends Schema {
  up () {
    this.create('regions', (table) => {
      table.increments()
      table.string('name', 100)
      table.string('ordinal', 100)
      table.timestamps()
    })
  }

  down () {
    this.drop('regions')
  }
}

module.exports = RegionSchema
