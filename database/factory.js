'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory');
const Hash = use('Hash');

Factory.blueprint('App/Models/User', (faker) => {
  return {
    username: faker.username(),
    email: 'admin@admin.com',
    password: Hash.make('123456789'),
  };
});
